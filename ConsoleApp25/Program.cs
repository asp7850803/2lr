﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Xml;

using (HttpClient client = new HttpClient())
{
    HttpResponseMessage response = await client.GetAsync("https://jsonplaceholder.typicode.com/posts/1");
    if (response.IsSuccessStatusCode)
    {
        string content = await response.Content.ReadAsStringAsync();
        Console.WriteLine(content);
    }
    else
    {
        Console.WriteLine($"Помилка: {response.StatusCode}");
    }
}


string passAttributeValue="";
string nameAttributeValue="";
XmlDocument xmlDoc2 = new XmlDocument();
xmlDoc2.Load("example.xml");
XmlNode rootElement = xmlDoc2.SelectSingleNode("/RootElement");
XmlNodeList childElements = rootElement.SelectNodes("ChildElement");
foreach (XmlNode childElement in childElements)
{
   passAttributeValue = childElement.Attributes["Pass"]?.Value;
   nameAttributeValue = childElement.Attributes["Name"]?.Value;
}



using (SmtpClient smtpClient = new SmtpClient("smtp.gmail.com", 587))
{
    smtpClient.Port = 587;
    smtpClient.Credentials = new NetworkCredential(nameAttributeValue, passAttributeValue);
    smtpClient.EnableSsl = true;
    MailMessage mailMessage = new MailMessage("patriot120104@gmail.com", "patriot120104@gmail.com", "Subject", "Body");
    smtpClient.Send(mailMessage);
}

string input = "Hello, World!";
using (MD5 md5 = MD5.Create())
{
    byte[] hashBytes = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
    string hashString = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
    Console.WriteLine($"MD5 Hash: {hashString}");
}


Process[] processes = Process.GetProcessesByName("chrome");
foreach (Process chromeProcess in processes)
{
    Console.WriteLine($"Process Name: {chromeProcess.ProcessName}, ID: {chromeProcess.Id}");
}